<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_changer' => 'Changer le statut de ces articles',

	// C
	'champ_rubrique' => 'Les articles de la rubrique :',
	'champ_nouveau_statut' => 'Choisir le nouveau statut',
	'champ_date_debut' => 'Publiés après : (inclus)',
	'champ_date_fin' => 'Publiés avant : (inclus)',
	'champ_statut' => 'Ayant le statut :',

	// M
	'menu_changer_statuts' => 'Statuts d\'articles',

	// S
	'statut_articles_modifies' => 'Les statuts des articles ont été modifiés',
	'statut_articles_titre' => 'Statut articles',

	// T
	'titre_changer_statuts' => 'Changer des statuts d\'articles',
);
